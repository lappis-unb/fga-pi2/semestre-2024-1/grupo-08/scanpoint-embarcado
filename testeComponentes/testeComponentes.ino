#include <Wire.h>
#include <Adafruit_VL53L0X.h>

// Define os pinos para o motor 1 prato
const int stepPin1 = 7; // Rotação
const int dirPin1 = 4; // Direção

// Define os pinos para o motor 2 do sensor
const int stepPin2 = 8; // Rotação
const int dirPin2 = 2; // Direção

// Botão que identifica que o motor checgou no ponto inicial
const int detectReset = 3;

// Define o número de passos por volta
const int stepsPerRevolution = 200;

// Cria um objeto para o sensor VL53L0X
Adafruit_VL53L0X lox = Adafruit_VL53L0X();

// IMPORTANTE! IMPORTANTE! IMPORTANTE! IMPORTANTE! 
// Antes de rodar esse arquivo de teste certifique 
// que a altura do sensor esteja no meio do eixo;
//

bool scanning = true;

void setup() {
  // Define os pinos como saídas
  pinMode(stepPin1, OUTPUT);
  pinMode(dirPin1, OUTPUT);
  pinMode(stepPin2, OUTPUT);
  pinMode(dirPin2, OUTPUT);
  pinMode(detectReset, INPUT);

  // Inicializa a comunicação serial
  Serial.begin(9600);

  // Descomentar delay se tiver problema com inicialização do sensor
  // delay(2000);
  // Inicializa o sensor VL53L0X
  if (!lox.begin()) {
    Serial.println("Falha ao iniciar o sensor infravermelho");
    while (1);
  }
}

void loop() {

    int stop = 0;

    if (scanning) {
        do{
        
            stop = 0;  

            // Faz 200 pulsos para completar uma volta do motor do sensor
            for (int x = 0; x < stepsPerRevolution; x++) {
                // 150 ms delay for half step
                moveMotor(1,0);
                stop += lerSensor();

            }

            // Faz 200 pulsos para completar uma volta do motor do prato
            for (int x = 0; x < 0.1*stepsPerRevolution; x++) {
                moveMotor(2,0);
            }
            
            // Delay de 1 segundo antes de reiniciar o ciclo
            delay(1000);

        } while (stop == 200);

        Serial.println("Stop");
        // Marca a varredura como concluída
        scanning = false;

    }
}

void moveMotor(int motorNumber, int anticlockwise){
  if(anticlockwise ==1 ){
    digitalWrite(dirPin1, LOW);
    digitalWrite(dirPin2, LOW);
  } else {
    digitalWrite(dirPin1, HIGH); 
    digitalWrite(dirPin2, HIGH); 
  }
  if(motorNumber == 1){ // Altura
    digitalWrite(stepPin1, HIGH);
    delay(150); // 150 ms delay for half step
    digitalWrite(stepPin1, LOW);
    delay(150); 
  }
    digitalWrite(stepPin2, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin2, LOW);
    delayMicroseconds(500);
}

int lerSensor() {
  VL53L0X_RangingMeasurementData_t measure;
  
  lox.rangingTest(&measure, true); // pass in 'true' to get debug data printout!
  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
    Serial.println(measure.RangeMilliMeter);
    return 0;
  } else {
    return 1;
  }
}
