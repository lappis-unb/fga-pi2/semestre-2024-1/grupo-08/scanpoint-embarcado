// Commands from serial
const String C_STOP = "C_STOP";
const String C_RESTART = "C_RESTART";
const String C_SCAN = "C_SCAN";
const String C_TEST = "C_TEST";
const String C_STATUS = "C_STATUS";
const String C_CANCEL = "C_CANCEL";

// Arduino response
const String A_TEST = "A_TEST";
const String A_SCAN = "A_SCAN";
const String A_RESTART = "A_RESTART";
const String A_READY = "A_READY";
const String A_ERROR_SENSOR = "A_ERROR_SENSOR";
const String A_ERROR_MOTOR = "A_ERROR_MOTOR";
const String A_STOP = "A_STOP";

char STATE_TESTING = 'T';
char STATE_SCANNING = 'V';
char STATE_ERROR_MOTOR = 'M';
char STATE_ERROR_SENSOR = 'I';
char STATE_STOP = 'S';
char STATE_RESTART = 'R';

char stateStack[4] = {'\0','\0','\0','\0'};
int currentStateIndex = 0;
bool responded = true;
bool testDone = false;
bool sensorInPosition = false; // botão da altura apertado
bool finishedCurrentState = true;

unsigned long startMillis;
unsigned long currentMillis;
const unsigned long period = 800; // the value is a number of milliseconds, ie 1 second

int tempStepMotor1 = 0;
int tempStepMotor2 = 0;
int motorToMoveOnScan = 1;
int emptyScan = 0;
int scanLayer = 0;

void setup() {

  Serial.begin(9600);
  startMillis = millis();
  Serial.println("Mock incialização com sensor");
  delay(1000);
   Serial.println("Setup finalizado");
}

void loop() {

 
  if(stateStack[currentStateIndex] == STATE_ERROR_MOTOR || stateStack[currentStateIndex] == STATE_ERROR_SENSOR){
    if(!responded && stateStack[currentStateIndex] == STATE_ERROR_MOTOR){
      Serial.println(A_ERROR_MOTOR);
      responded = true;
    }
    if(!responded && stateStack[currentStateIndex] == STATE_ERROR_SENSOR){
      Serial.println(A_ERROR_SENSOR);
      responded = true;
    }
    return ;
  }
 
  if(stateStack[currentStateIndex] == STATE_STOP){
    if (!responded) {
      Serial.println(A_STOP);
      responded = true;
    }
    return;
  }
  if(stateStack[currentStateIndex] == STATE_TESTING){

    if (!responded) {
      Serial.println(A_TEST);
      responded = true;
      finishedCurrentState = false;
      testDone = false;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
    }
    
    if(tempStepMotor1 != 200){
      // Test motor 1
      tempStepMotor1++;
    }

    if(tempStepMotor1 == 200 && tempStepMotor2 != 200){
      // Test motor 2
      tempStepMotor2++;
    }

    if(tempStepMotor1 == 200 && tempStepMotor2 == 200){
      // test Sensor
      finishedCurrentState = true;
    }
 
    //Mock tests with delay
    if(finishedCurrentState){
      testDone = true;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      popState();
    }
  }
  if(stateStack[currentStateIndex] == STATE_RESTART){
    if (!testDone) {
      popState();
      pushState(STATE_TESTING);
      responded = false;
      return;
    }
    if (!responded) {
      sensorInPosition = false;
      tempStepMotor1 = 0;
      finishedCurrentState = false;
      responded = true;
      Serial.println(A_RESTART);
    }
      //Mover até achar o botão ou mover 5 cm    
      if(tempStepMotor1 == 3000 || sensorInPosition){
        finishedCurrentState = true;
      } else {
        tempStepMotor1 ++;
      }
      
      // Reiniciar posição do sensor
      //Mock restart with delay
      delay(1000);
      if(finishedCurrentState){
        //Coment this when button is implemented
        sensorInPosition = true;
        if(!sensorInPosition){
          popState();
          pushState(STATE_ERROR_MOTOR);
          Serial.println(A_ERROR_MOTOR);
        }
        popState();
      }
  }
  if(stateStack[currentStateIndex] == STATE_SCANNING){
    if (!testDone) {
      popState();
      pushState(STATE_TESTING);
      responded = false;
      return;
    }
    if (!sensorInPosition) {
      popState();
      pushState(STATE_RESTART);
      responded = false;
      return;
    }
    if (!responded) {
      Serial.println(A_SCAN);
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      finishedCurrentState = false;
      responded = true;
      motorToMoveOnScan = 1;
      scanLayer = 0;
      emptyScan = 0;
    }


    if(tempStepMotor1 != 200 && motorToMoveOnScan == 1){
      // Test motor 1
      tempStepMotor1++;
    }

    if(tempStepMotor1 == 200 && motorToMoveOnScan == 1){
      tempStepMotor1 = 0;
      motorToMoveOnScan = 2;
      emptyScan = 0;
      scanLayer++;
    }

    if(tempStepMotor2 != 200 && motorToMoveOnScan == 2){
      // Test motor 2
      tempStepMotor2++;
    }

    if(tempStepMotor1 == 200 && motorToMoveOnScan == 1){
      tempStepMotor2 = 0;
      motorToMoveOnScan = 1;
    }


    //Mock scan with delay
    delay(1000);
    if (emptyScan == 200 || scanLayer == 38 ) {
      popState();
      scanLayer=0;
      emptyScan = 0;
      motorToMoveOnScan = 1;
      finishedCurrentState = true;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      responded = false;
      pushState(STATE_RESTART);
    }
  }

  if(!responded && testDone && sensorInPosition){
    Serial.println(A_READY);
    responded = true;
  }

}

void serialEvent() {
  while (Serial.available()) {

    String str = Serial.readStringUntil('\n');
    str.trim();
    Serial.print("Recebido: ");
    Serial.println(str);
    changeStatusAndRespond(str);
  }
}

void changeStatusAndRespond(String str) {
  if (!responded && str != C_STOP) {
    return;
  }
  responded = false;
  switch (commandStrToInt(str)) {
    case 1: {
      if (checkEmpty()) {
        pushState(STATE_STOP);
        return;
      }

      while (stateStack[currentStateIndex] != STATE_STOP && !checkEmpty()) {
        popState();
      }

      pushState(STATE_STOP);
      return;
      }
    case 2: { // restart
     
      if (sensorInPosition && testDone) {
        return;
      }

      if (stateStack[currentStateIndex] == STATE_STOP) {
        popState();
      }

      if (checkEmpty()) {
        pushState(STATE_RESTART);
      }

      if (!testDone) {
        pushState(STATE_TESTING);
        return;
      }
      
      return;
      }
    case 3: { //Scanning
      if (!checkEmpty()){
        return;
      }
      if (sensorInPosition && testDone) {
        pushState(STATE_SCANNING);
        return;
      }

      if (!sensorInPosition) {
        pushState(STATE_RESTART);
      }

      if (!testDone) {
        pushState(STATE_TESTING);
        return;
      }

      return;
      }
    case 4: { //Test
      if (checkEmpty()) {
        pushState(STATE_TESTING);
        return;
      }
      if (stateStack[currentStateIndex] == STATE_STOP) {
        testDone = false;
        popState();
        pushState(STATE_TESTING);
        return;
      } 

      if (stateStack[currentStateIndex] == STATE_TESTING || stateStack[currentStateIndex] == STATE_SCANNING ||
          stateStack[currentStateIndex] == STATE_RESTART) {
        return;
      }
      while (stateStack[currentStateIndex] != STATE_TESTING && !checkEmpty()) {
        popState();
      }
      pushState(STATE_TESTING);
      return;
      }
    case 5: {
      if (stateStack[currentStateIndex] == STATE_SCANNING) {
        popState();
        pushState(STATE_RESTART);
        return;
      }
      return;
      }
    default: {
      if (checkEmpty() && testDone && sensorInPosition) {
        Serial.print(A_READY);
        responded = true;
        return;
      }

      printStateStack();
//      Serial.print(stateStack[currentStateIndex]);
      responded = true;
      return;
      }
  }
}

int commandStrToInt(String str){
  if(str == C_STOP){
    return 1;
  }
  if(str == C_RESTART){
    return 2;
  }
  if(str == C_SCAN){
    return 3;
  }
  if(str == C_TEST){
    return 4;
  }
  if(str == C_CANCEL){
    return 5;
  }
  // default C_STATUS
  return 6;
}

void pushState(char stateToPush){
  if (currentStateIndex == 3) {
    return;
  }

  if(currentStateIndex!=0){
    currentStateIndex++; 
  }
  
  stateStack[currentStateIndex] = stateToPush;
}

void popState(){
  stateStack[currentStateIndex] = '\0';
  if (currentStateIndex == 0) {
    return;
  }
  currentStateIndex--;
}

void printStateStack(){
  int temp = currentStateIndex;
  Serial.println("Imprimindo estados");
  while(temp >=0 ) {
    Serial.print("Indice:");
    Serial.println(temp);
    Serial.print("Estado");
    Serial.println(stateStack[temp]);
    Serial.println("-----");
    temp--;
  }
  Serial.println("Terminado imprimindo estado");
  delay(2000);
}

bool checkEmpty(){
  return currentStateIndex == 0 && stateStack[0] == '\0';
}
