#include <SharpIR.h>
#include <Wire.h>

// Define os pinos para o motor 1 prato
const int stepPin1 = 7;
const int dirPin1 = 4;

// Define os pinos para o motor 2 altura
const int stepPin2 = 8;
const int dirPin2 = 2;

const int detectReset = 3;
const int sensorPin = A0;

SharpIR sensor( SharpIR::GP2Y0A41SK0F, A0 );

// Define o número de passos por volta
const int stepsPerRevolution = 200;

// Variável para controlar se a varredura está em andamento
bool scanning = false;

char stateStack[4] = {'\0','\0','\0','\0'};
int currentStateIndex = 0;
bool responded = true;
bool testDone = true;
bool sensorInPosition = false; // botão da altura apertado
bool finishedCurrentState = true;

int tempStepMotor1 = 0;
int tempStepMotor2 = 0;
int motorToMoveOnScan = 1;
int emptyScan = 0;
int scanLayer = 0;
int randomNumber = 0;

// Commands from serial
const String C_STOP = "C_STOP";
const String C_RESTART = "C_RESTART";
const String C_SCAN = "C_SCAN";
const String C_TEST = "C_TEST";
const String C_STATUS = "C_STATUS";
const String C_CANCEL = "C_CANCEL";

// Arduino response
const String A_TEST = "A_TEST";
const String A_SCAN = "A_SCAN";
const String A_RESTART = "A_RESTART";
const String A_READY = "A_READY";
const String A_ERROR_SENSOR = "A_ERROR_SENSOR";
const String A_ERROR_MOTOR = "A_ERROR_MOTOR";
const String A_STOP = "A_STOP";
const String A_FINISHED = "A_FINISHED";

char STATE_TESTING = 'T';
char STATE_SCANNING = 'V';
char STATE_ERROR_MOTOR = 'M';
char STATE_ERROR_SENSOR = 'I';
char STATE_STOP = 'S';
char STATE_RESTART = 'R';


void setup() {
  // Define os pinos como saídas
  pinMode(stepPin1, OUTPUT);
  pinMode(dirPin1, OUTPUT);
  pinMode(stepPin2, OUTPUT);
  pinMode(dirPin2, OUTPUT);
  pinMode(detectReset, INPUT_PULLUP);

  // Inicializa a comunicação serial
  Serial.begin(9600);

  pushState(STATE_RESTART);
  pushState(STATE_TESTING);
}

void loop() { 
  
  if(stateStack[currentStateIndex] == STATE_ERROR_MOTOR || stateStack[currentStateIndex] == STATE_ERROR_SENSOR){
    if(!responded && stateStack[currentStateIndex] == STATE_ERROR_MOTOR){
      Serial.println(A_ERROR_MOTOR);
      testDone=false;
      responded = true;
      sensorInPosition = false;
    }
    if(!responded && stateStack[currentStateIndex] == STATE_ERROR_SENSOR){
      Serial.println(A_ERROR_SENSOR);
      testDone=false;
      responded = true;
      sensorInPosition = false;
    }
    return ;
  }
 
  if(stateStack[currentStateIndex] == STATE_STOP){
    if (!responded) {
      Serial.println(A_STOP);
      responded = true;
    }
    return;
  }
  if(stateStack[currentStateIndex] == STATE_TESTING){

    if (!responded) {
      Serial.println(A_TEST);
      responded = true;
      finishedCurrentState = false;
      testDone = false;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
    }
    
    if(tempStepMotor1 != 200){
      // Test motor 1
      tempStepMotor1++;
    }

    if(tempStepMotor1 == 200 && tempStepMotor2 != 200){
      // Test motor 2
      tempStepMotor2++;
    }

    if(tempStepMotor1 == 200 && tempStepMotor2 == 200){
      // test Sensor
      finishedCurrentState = true;
    }
 
    //Mock tests with delay
    if(finishedCurrentState){
      testDone = true;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      popState();
    }
  }
  if(stateStack[currentStateIndex] == STATE_RESTART){
    if (!testDone) {
      popState();
      pushState(STATE_TESTING);
      responded = false;
      return;
    }
    if (!responded) {
      sensorInPosition = false;
      tempStepMotor2 = 0;
      finishedCurrentState = false;
      responded = true;
      Serial.println(A_RESTART);
    }
        
       if(digitalRead(detectReset) == LOW){
         sensorInPosition = true;
       } else if(!sensorInPosition) {
         moveMotor(2,1);
       }

      //Mover até achar o botão ou mover 5 cm    
      if(tempStepMotor2 == 11250 || sensorInPosition){
        finishedCurrentState = true;
      } else {
        tempStepMotor2 ++;
      }
      
      // Reiniciar posição do sensor
      //Mock restart with delay
      if(finishedCurrentState){
        if(!sensorInPosition){
          popState();
          pushState(STATE_ERROR_MOTOR);
          Serial.println(A_ERROR_MOTOR);
          return;
        }
        sensorInPosition=true;
        Serial.println(A_READY);
        popState();
      }
  }
  if(stateStack[currentStateIndex] == STATE_SCANNING){
    if (!testDone) {
      popState();
      pushState(STATE_TESTING);
      responded = false;
      return;
    }
    if (!sensorInPosition && finishedCurrentState==true) {
      popState();
      pushState(STATE_RESTART);
      responded = false;
      return;
    }
    if (!responded) {
      Serial.println(A_SCAN);
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      finishedCurrentState = false;
      responded = true;
      motorToMoveOnScan = 1;
      sensorInPosition = false;
      scanLayer = 0;
      emptyScan = 0;
    }

    if(tempStepMotor1 != 200 && motorToMoveOnScan == 1){ // Mexe prato e scaneia
      moveMotor(1,0);
      tempStepMotor1++;
      emptyScan += lerSensor();
    }

    if(tempStepMotor1 == 200 && motorToMoveOnScan == 1){
      tempStepMotor1 = 0;
      motorToMoveOnScan = 2;
      Serial.println(9999);
      scanLayer++;
    }

    if(tempStepMotor2 != 250 && motorToMoveOnScan == 2){
      moveMotor(2,0);
      tempStepMotor2++;
    }

    if(tempStepMotor2 == 250 && motorToMoveOnScan == 2){
      tempStepMotor2 = 0;
      emptyScan = 0;
      motorToMoveOnScan = 1;
    }

    if (emptyScan == 200 || scanLayer == 228 ) {
      popState();
      scanLayer=0;
      emptyScan = 0;
      motorToMoveOnScan = 1;
      finishedCurrentState = true;
      tempStepMotor1 = 0;
      tempStepMotor2 = 0;
      responded = false;
      Serial.println(A_FINISHED);
      pushState(STATE_RESTART);
    }
  }

  if(!responded && testDone && sensorInPosition){
    Serial.println(A_READY);
    responded = true;
  }

}

void moveMotor(int motorNumber, int anticlockwise){
  if(anticlockwise ==1 ){
    digitalWrite(dirPin1, LOW);
    digitalWrite(dirPin2, LOW);
  } else {
    digitalWrite(dirPin1, HIGH); 
    digitalWrite(dirPin2, HIGH); 
  }
  if(motorNumber == 2){ // Altura
    digitalWrite(stepPin2, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin2, LOW);
    delayMicroseconds(500);
    return;
  }
  digitalWrite(stepPin1, HIGH);
  delay(150); // 150 ms delay for half step
  digitalWrite(stepPin1, LOW);
  delay(150); 
    
}

int lerSensor() {
  float somaMedidas = 0;
  float numMedidasValidas = 0;
  int contaLoop = 0;

  int numero1 = 0;
  int sumNumero = 1;
  int numero2 = 0;
  int sumNumero2 = 0;

   while(numMedidasValidas<50 && contaLoop != 100){

    float cm = 10650.08 * pow(analogRead(sensorPin),-0.935) - 10;
    delay(21);
   if(cm > 20 && cm  < 60){
     numMedidasValidas++;
     somaMedidas += cm;
     } else {
       contaLoop++;
     }
   }
   float media = somaMedidas/numMedidasValidas;
    
   if(numMedidasValidas < 40 || media != media){
     Serial.println(0);
     return 1;
   } else {
     Serial.println(media);
     return 0;
   }

}


void serialEvent() {
  while (Serial.available()) {

    String str = Serial.readStringUntil('\n');
    str.trim();
    changeStatusAndRespond(str);
  }
}

void changeStatusAndRespond(String str) {
  if (!responded && str != C_STOP) {
    return;
  }
  responded = false;
  switch (commandStrToInt(str)) {
    case 1: {
      if (checkEmpty()) {
        pushState(STATE_STOP);
        return;
      }

      while (stateStack[currentStateIndex] != STATE_STOP && !checkEmpty()) {
        popState();
      }

      pushState(STATE_STOP);
      return;
      }
    case 2: { // restart
     
      if (sensorInPosition && testDone) {
        return;
      }

      if (stateStack[currentStateIndex] == STATE_STOP) {
        popState();
      }

      if (checkEmpty()) {
        pushState(STATE_RESTART);
      }

      if (!testDone) {
        pushState(STATE_TESTING);
        return;
      }
      
      return;
      }
    case 3: { //Scanning
      if (!checkEmpty()){
        return;
      }
      if (sensorInPosition && testDone) {
        pushState(STATE_SCANNING);
        return;
      }

      if (!sensorInPosition) {
        pushState(STATE_RESTART);
      }

      if (!testDone) {
        pushState(STATE_TESTING);
        return;
      }

      return;
      }
    case 4: { //Test
      if (checkEmpty()) {
        pushState(STATE_TESTING);
        return;
      }
      if (stateStack[currentStateIndex] == STATE_STOP) {
        testDone = false;
        popState();
        pushState(STATE_TESTING);
        return;
      } 

      if (stateStack[currentStateIndex] == STATE_TESTING || stateStack[currentStateIndex] == STATE_SCANNING ||
          stateStack[currentStateIndex] == STATE_RESTART) {
        return;
      }
      while (stateStack[currentStateIndex] != STATE_TESTING && !checkEmpty()) {
        popState();
      }
      pushState(STATE_TESTING);
      return;
      }
    case 5: {
      if (stateStack[currentStateIndex] == STATE_SCANNING) {
        popState();
        pushState(STATE_RESTART);
        return;
      }
      return;
      }
    default: {
      if (checkEmpty() && testDone && sensorInPosition) {
        Serial.print(A_READY);
        responded = true;
        return;
      }

      Serial.print(stateStack[currentStateIndex]);
      responded = true;
      return;
      }
  }
}

int commandStrToInt(String str){
  if(str == C_STOP){
    return 1;
  }
  if(str == C_RESTART){
    return 2;
  }
  if(str == C_SCAN){
    return 3;
  }
  if(str == C_TEST){
    return 4;
  }
  if(str == C_CANCEL){
    return 5;
  }
  // default C_STATUS
  return 6;
}

void pushState(char stateToPush){
  if (currentStateIndex == 3) {
    return;
  }

  if(currentStateIndex!=0){
    currentStateIndex++; 
  }
  
  stateStack[currentStateIndex] = stateToPush;
}

void popState(){
  stateStack[currentStateIndex] = '\0';
  if (currentStateIndex == 0) {
    return;
  }
  currentStateIndex--;
}


bool checkEmpty(){
  return currentStateIndex == 0 && stateStack[0] == '\0';
} 
